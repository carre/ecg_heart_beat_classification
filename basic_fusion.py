#!/usr/bin/env python_code

"""
basic_fusion.py
    
VARPA, University of Coruna
Mondejar Guerra, Victor M.
30 Oct 2017
"""
import os

from python_code.cross_validation import *
from python_code.feature_selection import *

from sklearn.preprocessing import MinMaxScaler

# Compute the basic rule from the list of probs 
# selected by rule index:
# 0 = product
# 1 = sum
# 2 = minimum
# 3 = minimum
# 4 = majority
# and return the predictions


def basic_rules(probs_ensemble, rule, weights=None):

    n_ensembles, n_instances, n_classes = probs_ensemble.shape
    predictions_rule = np.zeros(n_instances)

    # Product rule
    if rule == 'product':
        probs_rule = np.ones([n_instances, n_classes])

        for p in range(n_instances):
            for e in range(n_ensembles):
                probs_rule[p] = probs_rule[p] * probs_ensemble[e,p]
        predictions_rule = np.argmax(probs_rule, axis=1)
    
    # Sum rule
    elif rule == 'sum':
        probs_rule = np.zeros([n_instances, n_classes])

        for p in range(n_instances):
            for e in range(n_ensembles):
                probs_rule[p] = probs_rule[p] + probs_ensemble[e,p]
            # predictions_rule[p] = np.argmax(probs_rule[p])   # slow
        predictions_rule = np.argmax(probs_rule, axis=1)
    
    # Minimum rule
    elif rule == 'min':
        probs_rule = np.ones([n_instances, n_classes])

        for p in range(n_instances):
            for e in range(n_ensembles):
                probs_rule[p] = np.minimum(probs_rule[p], probs_ensemble[e,p])
        predictions_rule = np.argmax(probs_rule, axis=1)

    # Maximum rule
    elif rule == 'max':
        probs_rule = np.zeros([n_instances, n_classes])

        for p in range(n_instances):
            for e in range(n_ensembles):
                probs_rule[p] = np.maximum(probs_rule[p], probs_ensemble[e,p])
        predictions_rule = np.argmax(probs_rule, axis=1)
    
    # Majority rule
    elif rule == 'majority':
        rank_rule = np.zeros([n_instances, n_classes])
        # Just simply adds the position of the ranking 
        for p in range(n_instances):

            for e in range(n_ensembles):
                rank = np.argsort(probs_ensemble[e, p])
                for j in range(n_classes):
                    rank_rule[p, rank[j]] = rank_rule[p, rank[j]] + j
        predictions_rule = np.argmax(rank_rule, axis=1)

    # Sum rule
    elif rule == 'weighted_sum':
        probs_rule = np.zeros([n_instances, n_classes])
        for p in range(n_instances):
            for e in range(n_ensembles):
                probs_rule[p] = probs_rule[p] + weights[e]*probs_ensemble[e, p]
        predictions_rule = np.argmax(probs_rule, axis=1)

    return predictions_rule


def basic_fusion(db_path, features, rule, ann=False, ann_path=None, verbose=True, write_file=True, normalize=False,
                 ds='DS2'):

    # Load labels
    eval_labels = np.loadtxt(os.path.join(db_path, 'labels',  ds + '_labels.csv'))

    # Configuration
    results_path = os.path.join(db_path, 'results', 'ovo', 'MLII', 'rm_bsln')

    fusion_path = os.path.join(results_path, 'fusion')
    # check if filepath exists
    if not os.path.isdir(fusion_path):
        os.mkdir(fusion_path)

    if ds == 'DS2':
        model_rr = os.path.join(results_path, 'maxRR', 'RR', 'norm_RR', 'weighted', 'test_C_0.001_decision_ovo.csv')
        model_wvl = os.path.join(results_path, 'wvlt', 'weighted', 'test_C_0.001_decision_ovo.csv')
        model_hos = os.path.join(results_path, 'HOS', 'weighted', 'test_C_0.001_decision_ovo.csv')
        model_our_morph = os.path.join(results_path, 'our_morph', 'weighted', 'test_C_0.001_decision_ovo.csv')

    if ann:
        if ann_path:
            model_ann = os.path.join(ann_path, 'test_y_pred_prob.csv')
        else:
            model_ann = os.path.join(db_path, 'results', 'ann', 'MLII', 'rm_bsln', 'maxRR', 'RR', 'norm_RR', 'HOS',
                                     'our_morph', 'wvlt', 'weighted', 'test_y_pred_prob.csv')

    # Load Predictions!
    prob_ovo_rr = np.loadtxt(model_rr)
    prob_ovo_wvl = np.loadtxt(model_wvl)
    prob_ovo_hos = np.loadtxt(model_hos)
    prob_ovo_our_morph = np.loadtxt(model_our_morph)
    if ann:
        prob_ann = np.loadtxt(model_ann)

    _, prob_ovo_rr_sig = ovo_voting_exp(prob_ovo_rr, 4)
    _, prob_ovo_wvl_sig = ovo_voting_exp(prob_ovo_wvl, 4)
    _, prob_ovo_hos_sig = ovo_voting_exp(prob_ovo_hos, 4)
    _, prob_ovo_our_morph = ovo_voting_exp(prob_ovo_our_morph, 4)

    # Normalize for comparison with ANN
    if normalize or rule == 'weighted_sum':
        scaler = MinMaxScaler()
        prob_ovo_rr_sig = scaler.fit_transform(prob_ovo_rr_sig)
        prob_ovo_wvl_sig = scaler.fit_transform(prob_ovo_wvl_sig)
        prob_ovo_hos_sig = scaler.fit_transform(prob_ovo_hos_sig)
        prob_ovo_our_morph = scaler.fit_transform(prob_ovo_our_morph)

    ##########################################################
    # Combine the predictions!
    ##########################################################

    probs_ensemble = []
    prob_ovo_rr_sig = np.expand_dims(prob_ovo_rr_sig, axis=0)
    prob_ovo_wvl_sig = np.expand_dims(prob_ovo_wvl_sig, axis=0)
    prob_ovo_hos_sig = np.expand_dims(prob_ovo_hos_sig, axis=0)
    prob_ovo_our_morph_sig = np.expand_dims(prob_ovo_our_morph, axis=0)

    if ann:
        prob_ann = np.expand_dims(prob_ann, axis=0)

    for feature in features:

        if feature == 'rr':
            if len(probs_ensemble) == 0:
                probs_ensemble = prob_ovo_rr_sig
            else:
                probs_ensemble = np.vstack((probs_ensemble, prob_ovo_rr_sig))

        elif feature == 'wvlt':
            if len(probs_ensemble) == 0:
                probs_ensemble = prob_ovo_wvl_sig
            else:
                probs_ensemble = np.vstack((probs_ensemble, prob_ovo_wvl_sig))

        elif feature == 'hos':
            if len(probs_ensemble) == 0:
                probs_ensemble = prob_ovo_hos
            else:
                probs_ensemble = np.vstack((probs_ensemble, prob_ovo_hos_sig))

        elif feature == 'our_morph':
            if len(probs_ensemble) == 0:
                probs_ensemble = prob_ovo_our_morph
            else:
                probs_ensemble = np.vstack((probs_ensemble, prob_ovo_our_morph_sig))

    if ann:
        if len(probs_ensemble) == 0:
            probs_ensemble = prob_ann
        else:
            probs_ensemble = np.vstack((probs_ensemble, prob_ann))

    ###########################################

    # apply rule

    if rule == 'weighted_sum':
        write_file = False
        perf_measures_stk = []
        fl_stk = []
        score_stk = []
        for w1 in range(1, 2):
            for w2 in range(1, 2):
                for w3 in range(1, 2):
                    for w4 in range(1, 2):
                        #for w5 in range(1, 5, 0.1):
                        weights5 = np.arange(0.1, 1.0, 0.1)
                        for w5 in weights5:
                            weights = [w1, w2, w3, w4, w5]

                            if len(set((w1, w2, w3, w4, w5))) == 1:  # skip if all weights are equal
                                continue

                            perf_measures, fl = _run_fusion(probs_ensemble, eval_labels, features, rule, fusion_path,
                                                            normalize, verbose, ann, write_file, ds, weights)
                            perf_measures_stk.append(perf_measures)
                            fl_stk.append(fl)
                            score_stk.append(perf_measures.Ijk)
                            print('ijk {0} fusion = {1:.3f} weights {2} in {3}'.format(rule, perf_measures.Ijk, weights,
                                                                                       fl))

        # get best fusion and write to file
        ijk_max_indx = score_stk.index(max(score_stk))
        perf_measures = perf_measures_stk[ijk_max_indx]
        fl = fl_stk[ijk_max_indx]
        print('ijk {0} fusion = {1:.3f} norm {2} stored in {3}'.format(rule, perf_measures.Ijk, normalize, fl))
        write_aami_results(perf_measures, fl)

    else:
        perf_measures, fl = _run_fusion(probs_ensemble, eval_labels, features, rule, fusion_path, normalize, verbose,
                                        ann, write_file, ds, weights=None)

    return perf_measures, fl


def _run_fusion(probs_ensemble, eval_labels, features, rule, fusion_path, normalize, verbose=True, ann=False,
                write_file=True, ds='DS2',  weights=None):
    """ helper function to simplify basic_fusion code """

    predictions_prob_rule = basic_rules(probs_ensemble, rule, weights)
    perf_measures = compute_aami_performance_measures(predictions_prob_rule.astype(int), eval_labels)
    feature_names = '_'.join(features)

    if weights:
        rule = rule + ''.join(str(e) for e in weights)

    if ann:
        fl = os.path.join(fusion_path, feature_names + '_ann_' + rule + '_rule_score_Ijk_' +
                          str(format(perf_measures.Ijk, '.3f')) + '_' + ds + '.txt')
    else:
        fl = os.path.join(fusion_path, feature_names + '_' + rule + '_rule_score_Ijk_' +
                          str(format(perf_measures.Ijk, '.3f')) + '_' + ds + '.txt')

    if write_file:
        write_aami_results(perf_measures, fl)

        if verbose:
            print('ijk {0} fusion = {1:.3f} norm {2} stored in {3}'.format(rule, perf_measures.Ijk, normalize, fl))

    return perf_measures, fl


if __name__ == '__main__':
    import argparse
    parser1 = argparse.ArgumentParser(description="Run processes")
    parser1.add_argument('--mode', type=int, default=1)
    parser1.add_argument('--path', type=str,
                         default='/Users/phild/repos_carre/AI/ecg_classification/data_processed/mitdb_svm_mondejar/')
    args_ = parser1.parse_args()

    data_path = args_.path
    mode = args_.mode

    # Reproduce Table 5 results
    #
    if mode == 0:
        # RR + Wavelets
        features = ['rr', 'wvlt']
        rule = ['sum']

    elif mode == 1:
        # best fusion model_name according to Mondejar et al. 2019
        features = ['rr', 'wvlt', 'hos', 'our_morph']
        rule = 'product'

    basic_fusion(data_path, features, rule)
