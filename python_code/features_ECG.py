#!/usr/bin/env python_code

"""
features_ECG.py
    
VARPA, University of Coruna
Mondejar Guerra, Victor M.
23 Oct 2017
"""

import scipy.stats
import pywt
import operator

from python_code.mit_db import *


def compute_RR_intervals(r_poses):
    # Input: the R-peaks from a signal
    # Return: the feature_engineering RR intervals
    #   (pre_RR, post_RR, local_RR, global_RR)
    #    for each beat
    features_RR = RR_intervals()

    pre_r = np.array([], dtype=int)
    post_r = np.array([], dtype=int)
    local_r = np.array([], dtype=int)
    global_r = np.array([], dtype=int)

    # Pre_R and Post_R
    pre_r = np.append(pre_r, 0)
    post_r = np.append(post_r, r_poses[1] - r_poses[0])
    for i in range(1, len(r_poses) - 1):
        pre_r = np.append(pre_r, r_poses[i] - r_poses[i - 1])
        post_r = np.append(post_r, r_poses[i + 1] - r_poses[i])
    pre_r[0] = pre_r[1]
    pre_r = np.append(pre_r, r_poses[-1] - r_poses[-2])
    post_r = np.append(post_r, post_r[-1])

    # Local_R: AVG from last 10 pre_r values
    for i in range(0, len(r_poses)):
        num = 0
        avg_val = 0
        for j in range(-9, 1):
            if j+i >= 0:
                avg_val = avg_val + pre_r[i+j]
                num = num + 1
        local_r = np.append(local_r, avg_val / float(num))

    # Global R AVG: from full past-signal
    # TODO: AVG from past 5 minutes = 108000 samples
    global_r = np.append(global_r, pre_r[0])
    for i in range(1, len(r_poses)):
        num = 0
        avg_val = 0

        for j in range( 0, i):
            if (r_poses[i] - r_poses[j]) < 108000:
                avg_val = avg_val + pre_r[j]
                num = num + 1
        global_r = np.append(global_r, avg_val / float(num))

    for i in range(0, len(r_poses)):
        features_RR.pre_r = np.append(features_RR.pre_r, pre_r[i])
        features_RR.post_r = np.append(features_RR.post_r, post_r[i])
        features_RR.local_r = np.append(features_RR.local_r, local_r[i])
        features_RR.global_r = np.append(features_RR.global_r, global_r[i])
        #features_RR.append([pre_r[i], post_r[i], local_r[i], global_r[i]])
            
    return features_RR


def compute_wavelet_descriptor(beat, family, level):
    # Compute the wavelet descriptor for a beat
    wave_family = pywt.Wavelet(family)
    coeffs = pywt.wavedec(beat, wave_family, level=level)
    feature_names = ['wav_' + str(i) for i in range(0, len(coeffs[0]))]
    return coeffs[0], feature_names


def compute_my_own_descriptor(beat, winL, winR):
    # Compute my descriptor based on amplitudes of several intervals
    R_pos = int((winL + winR) / 2)

    R_value = beat[R_pos]
    my_morph = np.zeros((4))
    my_morph_names = ['']*len(my_morph)
    y_values = np.zeros(4)
    x_values = np.zeros(4)
    # Obtain (max/min) values and index from the intervals
    [x_values[0], y_values[0]] = max(enumerate(beat[0:40]), key=operator.itemgetter(1))
    [x_values[1], y_values[1]] = min(enumerate(beat[75:85]), key=operator.itemgetter(1))
    [x_values[2], y_values[2]] = min(enumerate(beat[95:105]), key=operator.itemgetter(1))
    [x_values[3], y_values[3]] = max(enumerate(beat[150:180]), key=operator.itemgetter(1))
    
    x_values[1] = x_values[1] + 75
    x_values[2] = x_values[2] + 95
    x_values[3] = x_values[3] + 150
    
    # Norm data before compute distance
    x_max = max(x_values)
    y_max = max(np.append(y_values, R_value))
    x_min = min(x_values)
    y_min = min(np.append(y_values, R_value))
    
    R_pos = (R_pos - x_min) / (x_max - x_min)
    R_value = (R_value - y_min) / (y_max - y_min)
    for n in range(0, 4):
        x_values[n] = (x_values[n] - x_min) / (x_max - x_min)
        y_values[n] = (y_values[n] - y_min) / (y_max - y_min)
        x_diff = (R_pos - x_values[n]) 
        y_diff = R_value - y_values[n]
        my_morph[n] = np.linalg.norm([x_diff, y_diff])
        my_morph_names[n] = 'morph_interval_' + str(n)
        # TODO test with np.sqrt(np.dot(x_diff, y_diff))
    
    if np.isnan(my_morph[n]):
        my_morph[n] = 0.0

    return my_morph, my_morph_names


def compute_hos_descriptor(beat, n_intervals, lag):
    """ Compute the higher order statistics (HOS) for each beat divided into n_intervals
    # Skewness (3 cumulant) and kurtosis (4 cumulant)
    """

    hos_b = np.zeros(((n_intervals-1) * 2))
    hos_b_names = ['']*len(hos_b)
    for i in range(0, n_intervals-1):
        pose = (lag * (i+1))
        interval = beat[int(pose - (lag/2)):int(pose + (lag/2))]
        
        # Skewness
        hos_b_names[i] = 'skew_interval_' + str(i)
        hos_b[i] = scipy.stats.skew(interval, 0, True)
        if np.isnan(hos_b[i]):
            hos_b[i] = 0.0
            
        # Kurtosis
        hos_b_names[(n_intervals-1) + i] = 'kurtosis_interval_' + str(i)
        hos_b[int(n_intervals-1 + i)] = scipy.stats.kurtosis(interval, 0, False, True)
        if np.isnan(hos_b[(n_intervals-1) + i]):
            hos_b[(n_intervals-1) + i] = 0.0

    return hos_b, hos_b_names


def compute_Uniform_LBP(signal, neigh=8):
    uniform_pattern_list = np.array(
        [0, 1, 2, 3, 4, 6, 7, 8, 12, 14, 15, 16, 24, 28, 30, 31, 32, 48, 56, 60, 62, 63, 64, 96, 112, 120, 124, 126,
         127, 128,
         129, 131, 135, 143, 159, 191, 192, 193, 195, 199, 207, 223, 224, 225, 227, 231, 239, 240, 241, 243, 247, 248,
         249, 251, 252, 253, 254, 255])
    # Compute the uniform LBP 1D from signal with neigh equal to number of neighbours
    # and return the 59 histogram:
    # 0-57: uniform patterns
    # 58: the non uniform pattern
    # NOTE: this method only works with neigh = 8
    hist_u_lbp = np.zeros(59, dtype=float)

    avg_win_size = 2
    # NOTE: Reduce sampling by half
    #signal_avg = scipy.signal.resample(signal, len(signal) / avg_win_size)

    for i in range(neigh/2, len(signal) - neigh/2):
        pattern = np.zeros(neigh)
        ind = 0
        for n in range(-neigh/2,0) + range(1,neigh/2+1):
            if signal[i] > signal[i+n]:
                pattern[ind] = 1          
            ind += 1
        # Convert pattern to id-int 0-255 (for neigh == 8)
        pattern_id = int("".join(str(c) for c in pattern.astype(int)), 2)

        # Convert id to uniform LBP id 0-57 (uniform LBP)  58: (non uniform LBP)
        if pattern_id in uniform_pattern_list:
            pattern_uniform_id = int(np.argwhere(uniform_pattern_list == pattern_id))
        else:
            pattern_uniform_id = 58 # Non uniforms patternsuse

        hist_u_lbp[pattern_uniform_id] += 1.0

    return hist_u_lbp


def compute_lbp(signal, neigh=4):
    hist_u_lbp = np.zeros(np.power(2, neigh), dtype=float)

    avg_win_size = 2
    # TODO: use some type of average of the data instead the full signal...
    # Average window-5 of the signal?
    #signal_avg = average_signal(signal, avg_win_size)
    # signal_avg = scipy.signal.resample(signal, len(signal) / avg_win_size)

    for i in range(int(neigh/2), int(len(signal) - neigh/2)):
        pattern = np.zeros(neigh)
        ind = 0
        for n in range(-neigh/2, 0) + range(1, neigh/2+1):
            if signal[i] > signal[i+n]:
                pattern[ind] = 1          
            ind += 1
        # Convert pattern to id-int 0-255 (for neigh == 8)
        pattern_id = int("".join(str(c) for c in pattern.astype(int)), 2)

        hist_u_lbp[pattern_id] += 1.0

    return hist_u_lbp


def compute_hbf(beat):
    # https://docs.scipy.org/doc/numpy-1.13.0/reference/routines.polynomials.hermite.html
    # Support Vector Machine-Based Expert System for Reliable Heartbeat Recognition
    # 15 hermite coefficients!

    # coeffs_hbf = np.zeros(15, dtype=float)
    coeffs_HBF_3 = np.polynomial.hermfit(range(0,len(beat)), beat, 3) # 3, 4, 5, 6?
    coeffs_HBF_4 = np.polynomial.hermfit(range(0,len(beat)), beat, 4)
    coeffs_HBF_5 = np.polynomial.hermfit(range(0,len(beat)), beat, 5)
    #coeffs_HBF_6 = hermfit(range(0,len(beat)), beat, 6)
    coeffs_hbf = np.concatenate((coeffs_HBF_3, coeffs_HBF_4, coeffs_HBF_5))
    return coeffs_hbf