#!/usr/bin/env python_code

"""
train_SVM.py
    
VARPA, University of Coruna
Mondejar Guerra, Victor M.
23 Oct 2017
"""
from python_code.load_MITBIH import load_mit_db
from python_code.aggregation_voting_strategies import ovo_voting, ovo_voting_exp, ovo_voting_both
from python_code.evaluation_AAMI import compute_aami_performance_measures, write_aami_results

import json
from matplotlib import pyplot as plt
import numpy as np
import time
import os
import joblib
from sklearn.preprocessing import StandardScaler
from sklearn import svm
from eli5.permutation_importance import get_score_importances


def main(multi_mode='ovo', win_l=90, win_r=90, do_preprocess=True, use_weight_class=True, max_rr=True, use_rr=True,
         norm_rr=True, compute_morph={''}, oversamp_method='', pca_k=0, feature_selection='', do_cross_val='',
         c_value=0.001, gamma_value=0.0, reduced_DS=False, leads_flag=[1, 0], skip_feature_importance=False,
         db_path='/Users/phild/Data/ecg_classification/mondejar/mitdb/', features_path='feature_engineering',
         labels_path='labels', svm_models_path='svm_models', results_path='results', feature_type=None):

    print("\nRunning train_SVM.py for {} ...".format(feature_type))

    # set up paths
    labels_path = os.path.join(db_path, labels_path)
    if not os.path.isdir(labels_path):
        os.mkdir(labels_path)

    results_path = os.path.join(db_path, results_path)
    if not os.path.isdir(results_path):
        os.mkdir(results_path)

    global svm_model

    # Load train data
    [features_train, labels_train, _, feature_names] = load_mit_db('DS1', win_l, win_r, do_preprocess, max_rr, use_rr,
                                                                   norm_rr, compute_morph, db_path, reduced_DS,
                                                                   leads_flag, features_path)

    # Load test data
    [features_test, labels_test, _, _] = load_mit_db('DS2', win_l, win_r, do_preprocess, max_rr, use_rr, norm_rr,
                                                     compute_morph, db_path, reduced_DS, leads_flag, features_path)

    np.savetxt(labels_path + os.sep + 'DS2_labels.csv', labels_test.astype(int), '%.0f')

    if oversamp_method:
        raise NotImplementedError
        # oversamp_features_pickle_name = create_oversamp_name(reduced_ds, do_preprocess, compute_morph, win_l, win_r,
        #                                                      max_rr, use_rr, norm_rr, pca_k)
        # features_test, labels_train = perform_oversampling(oversamp_method, db_path + 'oversamp/python_mit',
        #                                                    oversamp_features_pickle_name, features_test, labels_train)

    # Note: scaler is fit using train data only: https://sebastianraschka.com/faq/docs/scale-training-test.html
    print('Scaling features to zero mean unit variance (Standard Scaler)')
    scaler = StandardScaler()
    scaler.fit(features_train)
    features_train = scaler.transform(features_train)
    features_test = scaler.transform(features_test)

    if feature_selection:
        raise NotImplementedError
        # print("Running feature selection")
        # best_features = 7
        # features_train, features_index_sorted = run_feature_selection(features_train, labels_train, feature_selection,
        #                                                               best_features)
        # features_test = features_test[:, features_index_sorted[0:best_features]]

    if pca_k > 0:
        raise NotImplementedError
        # start = time.time()
        #
        # print("Running IPCA " + str(pca_k) + "...")
        #
        # # Run PCA
        # IPCA = sklearn.decomposition.IncrementalPCA(pca_k, batch_size=pca_k) # gamma_pca
        # IPCA.fit(features_train)
        #
        # # Apply PCA on test data!
        # features_train = IPCA.transform(features_train)
        # features_test = IPCA.transform(features_test)
        # end = time.time()
        # print("Time running IPCA (rbf): " + str(format(end - start, '.2f')) + " sec" )

    # 2) Cross-validation:
    if do_cross_val:
        raise NotImplementedError
        # print("Running cross val...")
        # start = time.time()
        #
        # # TODO Save data over the k-folds and ranked by the best average values in separated files
        # perf_measures_path = create_svm_model_name(results_path + multi_mode, do_preprocess, max_rr, use_rr, norm_rr,
        #                                            compute_morph, use_weight_class, feature_selection, oversamp_method,
        #                                            leads_flag, reduced_ds, pca_k, '/')
        #
        # # TODO implement this method! check to avoid NaN scores....
        #
        # if do_cross_val == 'pat_cv':  # Cross validation with one fold per patient
        #     cv_scores, c_values = run_cross_val(features_train, labels_train, patient_num_beats_train, do_cross_val,
        #                                         len(patient_num_beats_train))
        #
        #     if not os.path.exists(perf_measures_path):
        #         os.makedirs(perf_measures_path)
        #     np.savetxt(perf_measures_path + '/cross_val_k-pat_cv_F_score.csv', (c_values, cv_scores.astype(float)), "%f")
        #
        # elif do_cross_val == 'beat_cv':  # cross validation by class id samples
        #     k_folds = {5}
        #     for k in k_folds:
        #         ijk_scores, c_values = run_cross_val(features_train, labels_train, patient_num_beats_train,
        #                                              do_cross_val, k)
        #         # TODO Save data over the k-folds and ranked by the best average values in separated files
        #         perf_measures_path = create_svm_model_name(results_path + multi_mode, do_preprocess, max_rr, use_rr,
        #                                                    norm_rr, compute_morph, use_weight_class, feature_selection,
        #                                                    oversamp_method, leads_flag, reduced_ds, pca_k, '/')
        #
        #         if not os.path.exists(perf_measures_path):
        #             os.makedirs(perf_measures_path)
        #         np.savetxt(perf_measures_path + '/cross_val_k-' + str(k) + '_Ijk_score.csv', (c_values, ijk_scores.astype(float)), "%f")
        #
        #     end = time.time()
        #     print("Time running Cross Validation: " + str(format(end - start, '.2f')) + " sec" )
    else:

        # 3) Train SVM model_name

        # TODO load best params from cross validation!

        use_probability = False

        svm_models_path = os.path.join(db_path, svm_models_path)
        if not os.path.isdir(svm_models_path):
            os.mkdir(svm_models_path)
        model_svm_path = svm_models_path + os.sep + multi_mode + '_rbf'
        model_svm_path = create_svm_model_name(model_svm_path, do_preprocess, max_rr, use_rr, norm_rr, compute_morph,
                                               use_weight_class, feature_selection, oversamp_method, leads_flag,
                                               pca_k, '_')

        if gamma_value != 0.0:
            model_svm_path = model_svm_path + '_C_' + str(c_value) + '_g_' + str(gamma_value) + '.joblib.pkl'
        else:
            model_svm_path = model_svm_path + '_C_' + str(c_value) + '.joblib.pkl'

        print("Training model on MIT-BIH DS1: " + model_svm_path + "...")

        if os.path.isfile(model_svm_path):
            # Load the trained model_name!
            svm_model = joblib.load(model_svm_path)
        else:
            class_weights = {}
            for c in range(4):
                class_weights.update({c: len(labels_train) / float(np.count_nonzero(labels_train == c))})

            # class_weight='balanced',
            if gamma_value != 0.0:  # NOTE 0.0 means 1/n_features default value
                svm_model = svm.SVC(C=c_value, kernel='rbf', degree=3, gamma=gamma_value,
                                    coef0=0.0, shrinking=True, probability=use_probability, tol=0.001,
                                    cache_size=200, class_weight=class_weights, verbose=False,
                                    max_iter=-1, decision_function_shape=multi_mode, random_state=None)
            else:
                svm_model = svm.SVC(C=c_value, kernel='rbf', degree=3, gamma='auto',
                                    coef0=0.0, shrinking=True, probability=use_probability, tol=0.001,
                                    cache_size=200, class_weight=class_weights, verbose=False,
                                    max_iter=-1, decision_function_shape=multi_mode, random_state=None)

            # Let's Train!
            start = time.time()
            svm_model.fit(features_train, labels_train)
            end = time.time()
            # TODO assert that the class_ID appears with the desired order,
            # with the goal of ovo make the combinations properly
            print('trained model_name saved to {}'.format(model_svm_path))
            print('training time: {0:.2f} sec'.format(end - start))

            # Export model_name: save/write trained SVM model_name
            joblib.dump(svm_model, model_svm_path)

            # TODO Export StandardScaler()

        #########################################################################
        # 4) Test SVM model_name
        print("Testing model on MIT-BIH DS2: " + model_svm_path + "...")

        ############################################################################################################
        # EVALUATION
        ############################################################################################################

        # Evaluate the model

        perf_measures_path = create_svm_model_name(results_path + os.sep + multi_mode, do_preprocess, max_rr,
                                                   use_rr, norm_rr, compute_morph, use_weight_class, feature_selection,
                                                   oversamp_method, leads_flag, pca_k, '/')

        print("Evaluation on DS2 using ovo_voting_exp...")
        perf_measures = eval_model(svm_model, features_test, labels_test, multi_mode, 'ovo_voting_exp',
                                   perf_measures_path, c_value, gamma_value, 'test_')
        print('jk-index = {0:.3f}'.format(perf_measures.Ijk))
        print('cohen k  = {0:.3f}'.format(perf_measures.kappa))

        if not skip_feature_importance:
            features_sorted = feature_importance_svm(features_test, labels_test, feature_names=feature_names,
                                                     n_top_features=10)
            feature_importance_data = os.path.join(perf_measures_path, 'feature_importance_'+feature_type)
            keys = features_sorted.keys()
            values = features_sorted.values()
            plt.bar(keys, values)
            plt.ylabel('jk-index contribution')
            plt.savefig(feature_importance_data + '.png')

            # save to text
            with open(feature_importance_data + '.txt', 'w') as file:
                file.write(json.dumps(features_sorted))

            print('feature importance figure saved to {}'.format(feature_importance_data + '.png'))
            print('feature importance data saved to {}'.format(feature_importance_data + '.txt'))


def create_svm_model_name(model_svm_path, do_preprocess, max_rr, use_rr, norm_rr, compute_morph, use_weight_class,
                          feature_selection, oversamp_method, leads_flag, pca_k, delimiter):

    if leads_flag[0] == 1:
        model_svm_path = model_svm_path + delimiter + 'MLII'
    
    if leads_flag[1] == 1:
        model_svm_path = model_svm_path + delimiter + 'V1'

    if oversamp_method: 
        model_svm_path = model_svm_path + delimiter + oversamp_method

    if feature_selection:
        model_svm_path = model_svm_path + delimiter + feature_selection

    if do_preprocess:
        model_svm_path = model_svm_path + delimiter + 'rm_bsln'

    if max_rr:
        model_svm_path = model_svm_path + delimiter + 'maxRR'

    if use_rr:
        model_svm_path = model_svm_path + delimiter + 'RR'
    
    if norm_rr:
        model_svm_path = model_svm_path + delimiter + 'norm_RR'
    
    for descp in compute_morph:
        if descp:
            model_svm_path = model_svm_path + delimiter + descp

    if use_weight_class:
        model_svm_path = model_svm_path + delimiter + 'weighted'

    if pca_k > 0:
        model_svm_path = model_svm_path + delimiter + 'pca_' + str(pca_k)

    return model_svm_path


def eval_model(svm_model, features, labels_true, multi_mode, voting_strategy, output_path, c_value, gamma_value, ds):
    """ Eval the SVM model_name and export the results

    Notes:
    - The decision function tells us on which side of the hyperplane generated by the classifier we are
      (and how far we are away from it)

    """

    if multi_mode == 'ovo':
        decision_ovo = svm_model.decision_function(features)

        if voting_strategy == 'ovo_voting':
            labels_predict, counter = ovo_voting(decision_ovo, 4)

        elif voting_strategy == 'ovo_voting_both':
            labels_predict, counter = ovo_voting_both(decision_ovo, 4)

        elif voting_strategy == 'ovo_voting_exp':
            labels_predict, counter = ovo_voting_exp(decision_ovo, 4)

        perf_measures = compute_aami_performance_measures(labels_predict, labels_true)

    # Write results and also predictions on DS2
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    if gamma_value != 0.0:
        write_aami_results(perf_measures, output_path + '/' + ds + 'C_' + str(c_value) + 'g_' + str(gamma_value) +
                           '_score_Ijk_' + str(format(perf_measures.Ijk, '.2f')) + '_' + voting_strategy + '.txt')
    else:
        write_aami_results(perf_measures, output_path + '/' + ds + 'C_' + str(c_value) +
                           '_score_Ijk_' + str(format(perf_measures.Ijk, '.2f')) + '_' + voting_strategy + '.txt')
    
    # Array to .csv
    if multi_mode == 'ovo':
        if gamma_value != 0.0:
            np.savetxt(output_path + '/' + ds + 'C_' + str(c_value) + 'g_' + str(gamma_value) +
                       '_decision_ovo.csv', decision_ovo)
            np.savetxt(output_path + '/' + ds + 'C_' + str(c_value) + 'g_' + str(gamma_value) +
                       '_predict_' + voting_strategy + '.csv', labels_predict.astype(int), '%.0f')
        else:
            np.savetxt(output_path + '/' + ds + 'C_' + str(c_value) +
                       '_decision_ovo.csv', decision_ovo)
            np.savetxt(output_path + '/' + ds + 'C_' + str(c_value) +
                       '_predict_' + voting_strategy + '.csv', labels_predict.astype(int), '%.0f')

    elif multi_mode == 'ovr':
        raise NotImplemented
        # np.savetxt(output_path + '/' + ds + 'C_' + str(c_value) + '_decision_ovr.csv', prob_ovr)
        # np.savetxt(output_path + '/' + ds + 'C_' + str(c_value) + '_predict_' + voting_strategy + '.csv',
        #            predict_ovr.astype(int), '%.0f')

    print("Results written to " + output_path + '/' + ds + 'C_' + str(c_value))

    return perf_measures


def create_oversamp_name(reduced_ds, do_preprocess, compute_morph, win_l, win_r, max_rr, use_rr, norm_rr, pca_k):
    oversamp_features_pickle_name = ''
    if reduced_ds:
        oversamp_features_pickle_name += '_reduced_'
        
    if do_preprocess:
        oversamp_features_pickle_name += '_rm_bsline'

    if max_rr:
        oversamp_features_pickle_name += '_maxRR'

    if use_rr:
        oversamp_features_pickle_name += '_RR'
    
    if norm_rr:
        oversamp_features_pickle_name += '_norm_RR'

    for descp in compute_morph:
        oversamp_features_pickle_name += '_' + descp

    if pca_k > 0:
        oversamp_features_pickle_name += '_pca_' + str(pca_k)
    
    oversamp_features_pickle_name += '_wL_' + str(win_l) + '_wR_' + str(win_r)
    
    return oversamp_features_pickle_name


def feature_importance_svm(features, labels, feature_names=None, verbose=True, n_top_features=5):
    """ calculate feature importance for a black box model
    https://towardsdatascience.com/how-to-find-feature-importances-for-blackbox-models-c418b694659d
    https://github.com/cerlymarco/MEDIUM_NoteBook/blob/master/NeuralNet_FeatureImportance/NeuralNet_FeatImportance.ipynb

    """
    if verbose:
        print('starting feature importance search...')
    score_fn = jk_index_score

    if np.ndim(labels) > 1:
        labels = np.argmax(labels, axis=1)

    base_score, score_decreases = get_score_importances(score_fn, np.array(features), labels)
    feature_importances = np.mean(score_decreases, axis=0)

    feature_importance_dict = {}
    for i, feature_name in enumerate(feature_names):
        feature_importance_dict[feature_name] = feature_importances[i]

    features_sorted = dict(sorted(feature_importance_dict.items(), key=lambda x: x[1], reverse=True)[:n_top_features])

    if verbose:
        print('Top {} features:'.format(n_top_features))
        for key, value in features_sorted.items():
            print('{0} with value {1:.3f}'.format(key, value))

    return features_sorted


def jk_index_score(x, y):
    y_pred = svm_model.predict(x)
    if np.ndim(y_pred) > 1:
        y_pred = np.argmax(y_pred, axis=1)
    perf_measures = compute_aami_performance_measures(y_pred, y)
    ijk = perf_measures.Ijk
    if np.isnan(ijk):
        ijk = 0
    return ijk

# if __name__ == '__main__':
#     import argparse
#     parser = argparse.ArgumentParser(description="Run processes")
#     parser.add_argument('--data_path', type=str, default='/Users/phild/Data/ecg_classification-classification-mondejar/')
#     args = parser.parse_args()
#     data_path = args.data_path
#
#     # Reproduce Table 4 results from Mondejar et al. 2019
#     #
#     for mode in (range(4, 5)):
#
#         if mode == 0:
#             # RR : AGREES WITH MONDEJAR PAPER
#             multi_mode = 'ovo'  # decision_function_shape for SVC (paper uses one-vs-one ‘ovo’)
#             win_l = 90  # n frames before beat peak to determine start of beat signal
#             win_r = 90  # n frames after beat peak to determine end of beat signal
#             do_preprocess = True  # apply baseline removal via two consecutive median filts
#             use_weight_class = True  # ??? Doesnt seem to affect processes
#             maxRR = True  # compute maximum RR-interval feature
#             use_RR = True  # must use RR interval
#             norm_RR = True  # must include normalized RR
#             compute_morph = {''}  # list of morphs to compute
#             oversamp_method = ''  # performing SMOTE. If empty do not SMOTE
#             pca_k = 0  # do not run pca analysis
#             feature_selection = ''  # do not perform feature selection
#             do_cross_val = ''  # do not run cross-val
#             c_value = 0.001
#             gamma_value = 0.0  # for SVC model_name. corresponds to 'auto' setting
#             reduced_ds = False  # if false, dataset from paper
#             leads_flag = [1, 0]  # MLII, V1
#
#         elif mode == 1:
#             # HOS : AGREES WITH MONDEJAR PAPER
#             multi_mode = 'ovo'  # decision_function_shape for SVC (paper uses one-vs-one ‘ovo’)
#             win_l = 90  # n frames before beat peak to determine start of beat signal
#             win_r = 90  # n frames after beat peak to determine end of beat signal
#             do_preprocess = True  # apply baseline removal via two consecutive median filts
#             use_weight_class = True  # ??? Doesnt seem to affect processes
#             maxRR = False  # compute maximum RR-interval feature
#             use_RR = False  # must use RR interval
#             norm_RR = False  # must include normalized RR
#             compute_morph = {'HOS'}  # list of morphs to compute
#             oversamp_method = ''  # performing SMOTE. If empty do not SMOTE
#             pca_k = 0  # do not run pca analysis
#             feature_selection = ''  # do not perform feature selection
#             do_cross_val = ''  # do not run cross-val
#             c_value = 0.001
#             gamma_value = 0.0  # for SVC model_name. corresponds to 'auto' setting
#             leads_flag = [1, 0]  # MLII, V1
#
#         elif mode == 2:
#             # wavelet : AGREES WITH MONDEJAR PAPER
#             multi_mode = 'ovo'  # decision_function_shape for SVC (paper uses one-vs-one ‘ovo’)
#             win_l = 90  # n frames before beat peak to determine start of beat signal
#             win_r = 90  # n frames after beat peak to determine end of beat signal
#             do_preprocess = True  # apply baseline removal via two consecutive median filts
#             use_weight_class = True  # ??? Doesnt seem to affect processes
#             maxRR = False  # compute maximum RR-interval feature
#             use_RR = False  # must use RR interval
#             norm_RR = False  # must include normalized RR
#             compute_morph = {'wvlt'}  # list of morphs to compute
#             oversamp_method = ''  # performing SMOTE. If empty do not SMOTE
#             pca_k = 0  # do not run pca analysis
#             feature_selection = ''  # do not perform feature selection
#             do_cross_val = ''  # do not run cross-val
#             c_value = 0.001
#             gamma_value = 0.0  # for SVC model_name. corresponds to 'auto' setting
#             leads_flag = [1, 0]  # MLII, V1
#
#         elif mode == 3:
#             # morph : AGREES WITH MONDEJAR PAPER
#             multi_mode = 'ovo'  # decision_function_shape for SVC (paper uses one-vs-one ‘ovo’)
#             win_l = 90  # n frames before beat peak to determine start of beat signal
#             win_r = 90  # n frames after beat peak to determine end of beat signal
#             do_preprocess = True  # apply baseline removal via two consecutive median filts
#             use_weight_class = True  # ??? Doesnt seem to affect processes
#             maxRR = False  # compute maximum RR-interval feature
#             use_RR = False  # must use RR interval
#             norm_RR = False  # must include normalized RR
#             compute_morph = {'our_morph'}  # list of morphs to compute
#             oversamp_method = ''  # performing SMOTE. If empty do not SMOTE
#             pca_k = 0  # do not run pca analysis
#             feature_selection = ''  # do not perform feature selection
#             do_cross_val = ''  # do not run cross-val
#             c_value = 0.001
#             gamma_value = 0.0  # for SVC model_name. corresponds to 'auto' setting
#             leads_flag = [1, 0]  # MLII, V1
#
#         elif mode == 4:
#             # LBP
#             print('cannot reproduce LBP result, not important...skipping')
#             continue
#
#         # main each model
#         main(multi_mode, win_l, win_r, do_preprocess, use_weight_class, maxRR, use_RR, norm_RR, compute_morph,
#                 oversamp_method, pca_k, feature_selection, do_cross_val, c_value, gamma_value, leads_flag)
#
#     # Best fusion model from Mondejar paper
#     features = ['rr', 'wvlt', 'hos', 'our_morph']
#     rule = 'product'
#     basic_fusion(data_path, features, rule)