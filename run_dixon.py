#!/usr/bin/env python_code

"""
run_dixon.py

Master script to run ANN and ANN+SVM fusion models reported in "Heartbeat classification using an ensemble of support
vector machine and artificial neural network methods". Dixon PC, Fournier P-A, Roy JF. under review by
Biomedical Signal Processing & Control

"""

""" import standard libraries """
import numpy as np
import os
import itertools
from matplotlib import pyplot as plt

""" import custom libraries """
from python_code.train_SVM import main
from basic_fusion import basic_fusion
from ann_anwar_tensorflow import ann_anwar_tensorflow

""" SETTINGS TO UPDATE """
DATA_PTH = '/Users/phild/Data/ecg-classification-mondejar/mitdb/'


def train_test_svm_ann(data_pth, skip_svm_train=False, skip_svm_fusion_train=False, skip_ann_train=False,
                       skip_feature_importance=False):
    """ performs training and testing for svm and ann models

    Arguments
    --------
    data_pth : str
        Full path to location where data is stored. Must be edited for local computer
    skip_svm_train : bool (default=False)
        If true, training of individual models is skipped. Used if models were previously trained and available
    skip_svm_fusion_train : bool (default=False)
        If true, training of svm fusion model is skipped. Used if model was previously trained and available
    skip_ann_train : bool (default=False)
        If true, training of ann model is skipped. Used if model was previously trained and available
    skip_feature_importance : bool (default=False)
        If true, compute feature importance for each model
    """

    """ FIXED SETTINGS """
    features = ['rr', 'wvlt', 'hos', 'our_morph']
    rules = ['sum', 'product', 'majority', 'min', 'max']
    class_names = ['N', 'SVB', 'VB', 'F']

    # 1) train/test the individual SVM models of Mondejar et al. 2019
    if not skip_svm_train:
        for feature in features:

            if feature == 'rr':
                # RR : AGREES WITH MONDEJAR PAPER
                max_rr = True  # compute maximum RR-interval feature
                use_rr = True  # must use RR interval
                norm_rr = True  # must include normalized RR
                compute_morph = {''}  # list of morphs to compute

            elif feature == 'hos':
                # HOS : AGREES WITH MONDEJAR PAPER
                max_rr = False  # compute maximum RR-interval feature
                use_rr = False  # must use RR interval
                norm_rr = False  # must include normalized RR
                compute_morph = {feature.upper()}  # list of morphs to compute

            elif feature == 'wvlt':
                # wavelet : AGREES WITH MONDEJAR PAPER
                max_rr = False  # compute maximum RR-interval feature
                use_rr = False  # must use RR interval
                norm_rr = False  # must include normalized RR
                compute_morph = {feature}  # list of morphs to comput

            elif feature == 'our_morph':
                # morph : AGREES WITH MONDEJAR PAPER
                max_rr = False  # compute maximum RR-interval feature
                use_rr = False  # must use RR interval
                norm_rr = False  # must include normalized RR
                compute_morph = {feature}  # list of morphs to compute

            # run each model
            main(max_rr=max_rr, use_rr=use_rr, norm_rr=norm_rr, compute_morph=compute_morph,
                 skip_feature_importance=skip_feature_importance, db_path=data_pth, feature_type=feature)

    # 2) train/test the 'best' fusion model from Mondejar et al. 2019 paper ijk = 0.773
    if not skip_svm_fusion_train:
        rule = 'product'
        perf_metric, result_pth = basic_fusion(data_pth, features, rule=rule)
        cm = perf_metric.confusion_matrix.astype(int)
        cm_pth = os.path.split(result_pth)[0]
        print_save_confusion_matrix(cm, class_names, cm_pth, 'mondejar_{}_fusion'.format(rule))

    # 3) train/test the individual ANN model
    if not skip_ann_train:
        ann_anwar_tensorflow(data_pth)

    # 4) train/test the new fusion SMV + ANN model
    ann_svm_fusion_ijk = np.zeros(len(rules),)
    perf_metrics = []
    result_pths = []
    for i, rule in enumerate(rules):
        perf_metric, result_pth = basic_fusion(data_pth, features, rule=rule, ann=True, verbose=False)
        perf_metrics.append(perf_metric)
        result_pths.append(result_pth)
        ann_svm_fusion_ijk[i] = perf_metric.Ijk

    # get best fusion model
    indx = np.argmax(ann_svm_fusion_ijk)
    model_name = 'ann_svm_fusion_{0}'.format(rules[indx])
    print('best ann+svm fusion model is {0} with jk-index = {1:.4f}'.format(model_name, perf_metrics[indx].Ijk))

    # print plot save confusion matrix
    cm = perf_metrics[indx].confusion_matrix.astype(int)
    cm_pth = os.path.split(result_pth)[0]
    print_save_confusion_matrix(cm, class_names, cm_pth, model_name)

    # print out other results
    print('Accuracy = {0:.4f}'.format(np.mean(perf_metrics[indx].Acc)))
    print('Sensitivity (recall) = {0:.3f}'.format(np.mean(perf_metrics[indx].Recall)))
    print('Specificity = {0:.4f}'.format(np.mean(perf_metrics[indx].Specificity)))
    print('Precision = {0:.4f}'.format(np.mean(perf_metrics[indx].Precision)))
    print('Cohen kappa = {0:.4f}'.format(perf_metrics[indx].kappa))
    print('jk-index = {0:.4f}'.format(perf_metrics[indx].Ijk))
    print(' ')
    print('full results stored in: {}'.format(result_pths[indx]))


def print_save_confusion_matrix(cm, classes, result_pth, model_name, cmap=plt.cm.Blues):
    """ Prints and saves confusion matrix cm

    Parameters:
    ----------
    cm : array, shape = [n_classes, n_classes]
        Confusion matrix
    classes : list
        Label categories in data set
    result_pth : str
        Full path to location to save confusion matrix to.
    model_name : str
        Name of model to append to file name
    cmap : matplotlib color map. (default= plt.cm.Blues)
        Determines color scheme used on cm plot
    """

    # PRINT
    print('confusion matrix:')
    print(cm)

    # SAVE
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="grey" if cm[i, j] > thresh else "black", fontsize=12)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45, fontsize=14)
    plt.yticks(tick_marks, classes, fontsize=14)
    plt.tight_layout()
    plt.ylabel('True label', fontsize=20)
    plt.xlabel('Predicted label', fontsize=20)
    plt.tight_layout()

    save_file = os.path.join(result_pth, 'confusion_matrix_{}.png'.format(model_name))
    plt.savefig(save_file, dpi=300)
    plt.close()


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description="Run Dixon study")
    parser.add_argument('--data_pth', type=str, default='')
    parser.add_argument('--skip_svm_train', help='to skip if svm mdls already trained', action='store_true')
    parser.add_argument('--skip_svm_fusion_train', help='to skip if fusion svm mdl aleady trained', action='store_true')
    parser.add_argument('--skip_ann_train', help='use if individual model files aleady trained', action='store_true')
    parser.add_argument('--skip_feature_importance', help='to skip feature importance calculation', action='store_true')

    args = parser.parse_args()

    if args.data_pth == '':
        data_pth = DATA_PTH
    else:
        data_pth = args.data_pth

    train_test_svm_ann(data_pth, skip_svm_train=args.skip_svm_train, skip_svm_fusion_train=args.skip_svm_fusion_train,
                       skip_ann_train=args.skip_ann_train, skip_feature_importance=args.skip_feature_importance)
