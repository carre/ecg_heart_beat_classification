
When using this code, please cite our paper:

Dixon, PC, Fournier PA (under review). Heartbeat classification using an ensemble of support vector machine and artificial neural network methods