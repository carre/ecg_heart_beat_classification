## Steps (How to run)
 
1. Download and unzip the MITDB dataset from Kaggle : https://www.kaggle.com/mondejar/mitbih-database

2. Install conda (if not already on your system) : https://docs.conda.io/en/latest/miniconda.html


3. Install virtual environment : 
    
   - open terminal
   - cd to root of repo
   - type : ``conda env create -f ecg.yml``

3. Set up virtual environment :
    - open termnal 
    - cd to root of repo
    - type ``conda activate ecg``
   
    
5. Run the code:
    - locate the file ``run_dixon.py`` in the root of the repo
    - adapt variable ``DATA_PTH`` to point to the location where the MITDB csv files are stored locally
    - Run the file in terminal ``$ python run_dixon.py``    

6. Notes
    
   - type ``deactivate`` to deactivate the virtual environment

   - ``run_dixon.py`` performs four tasks : 
        - Reproduces the results for the 'RR', HOS', 'wavelet', 'Our Morph.' SVM models 
          from Table 4 in Mondéjar et al. 2019.
        - Reproduces the results for the best model from Table 5 in Mondéjar et al. 2019. ('RR', HOS', 'wavelet', 
          'Our Morph' fusion using the product rule)
        - Runs the results for a novel ANN model based on the features of Mondéjar et al. 2019 (jk=0.610)
        - Runs the resuls for a novel ANN + SVM fusion model that outperforms the state-of-the-art (jk=0.826)
        
   - Present code is a forked version of the public code published by Mondéjar as a companion to a paper by Dixon et al. 
     currently under review. The original repo can be found at https://github.com/mondejar/ecg-classification/tree/master/python
     
   - Some unused functionality from the Mondéjar code was removed to avoid complexity and the need for the installation
     of additional python modules.
   
