#!/usr/bin/env python_code

"""
run_SVM.py
    
VARPA, University of Coruna
Mondejar Guerra, Victor M.
27 Oct 2017
"""
from python_code.train_SVM import main

# Call different configurations for train_SVM.py

# Settings
winL = 90                                   # size of the window centred at R-peak at left side
winR = 90                                   # size of the window centred at R-peak at right side
do_preprocess = True                        # apply baseline removal via two consecutive median filts (see load_signal)
use_weight_class = True
maxRR = True
compute_morph = {''}                        # e.g. 'wvlt', 'HOS', 'myMorph'

multi_mode = 'ovo'
voting_strategy = 'ovo_voting'              # 'ovo_voting_exp', 'ovo_voting_both'

use_RR = False                               # beat descriptors based on several R-R intervals
norm_RR = False                              # dividing R-R value by its mean value within the same ECG record

oversamp_method = ''                         # perform SMOTE oversampling, not used in paper (see perform_oversampling)
feature_selection = ''                       # perform feature selection, not used in paper (see run_feature_selection)
do_cross_val = ''
C_value = 0.001
reduced_ds = False                           # To select only patients in common with MLII and V1
leads_flag = [1, 0]                          # MLII, V1

pca_k = 0

ov_methods = {''}                            # 'SMOTE_regular'}

C_values = {0.001} #, 0.01, 0.1, 1, 10, 100}    # penalty parameter grid search see section 3, Mondejar et al. 2019
gamma_values = {0.0}
gamma_value = 0.0

for C_value in C_values:
    pca_k = 0

    # Phil D edit: best single model_name from Mondejar et al. 2019
    multi_mode = 'ovo'                         # decision_function_shape for SVC (paper uses one-vs-one ‘ovo’)
    win_l = 90                                 # n frames before beat peak to determine start of beat signal
    win_r = 90                                 # n frames after beat peak to determine end of beat signal
    do_preprocess = True                       # apply baseline removal via two consecutive median filts

    use_weight_class = True                    # ??? Doesnt seem to affect processes
    maxRR = True                               # compute maximum RR-interval feature
    use_RR = True                              # must use RR interval
    norm_RR = True                             # must include normalized RR
    compute_morph = {'HOS', 'wvlt', 'myMorph'} # list of morphs to compute
    oversamp_method = ''                       # performing SMOTE. If empty do not SMOTE
    pca_k = 0                                  # do not run pca analysis
    feature_selection = ''                     # do not perform feature selection
    do_cross_val = False
    gamma_value = 0.0                          # for SVC model_name. corresponds to 'auto' setting
    main(multi_mode, win_l, win_r, do_preprocess, use_weight_class, maxRR, use_RR, norm_RR, compute_morph,
         oversamp_method, pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)
    # end Phil D edit

    # use_RR = True
    # norm_RR = True
    # compute_morph = {'u-lbp'}
    # main(multi_mode, 90, 90, do_preprocess, use_weights, maxRR, use_RR, norm_RR, compute_morph, oversamp_method,
    #         pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)
    #
    # # Single
    # use_RR = False
    # norm_RR = False
    # compute_morph = {'u-lbp'}
    # main(multi_mode, 90, 90, do_preprocess, use_weights, maxRR, use_RR, norm_RR, compute_morph, oversamp_method,
    #         pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)
            
    """
    # Two
    use_RR = True
    norm_RR = True
    compute_morph = {'u-lbp'} 
    main(multi_mode, 90, 90, do_preprocess, use_weights, maxRR, use_RR, norm_RR, compute_morph, oversamp_method, pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)
    
    use_RR = False
    norm_RR = False
    compute_morph = {'wvlt', 'u-lbp'} 
    main(multi_mode, 90, 90, do_preprocess, use_weights, maxRR, use_RR, norm_RR, compute_morph, oversamp_method, pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)
            
    compute_morph = {'HOS', 'u-lbp'} 
    main(multi_mode, 90, 90, do_preprocess, use_weights, maxRR, use_RR, norm_RR, compute_morph, oversamp_method, pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)

    compute_morph = {'myMorph', 'u-lbp'} 
    main(multi_mode, 90, 90, do_preprocess, use_weights, maxRR, use_RR, norm_RR, compute_morph, oversamp_method, pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)
             


    # Three
    use_RR = True
    norm_RR = True
    compute_morph = {'wvlt', 'u-lbp'} 
    main(multi_mode, 90, 90, do_preprocess, use_weights, maxRR, use_RR, norm_RR, compute_morph, oversamp_method, pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)
            
    compute_morph = {'HOS', 'u-lbp'} 
    main(multi_mode, 90, 90, do_preprocess, use_weights, maxRR, use_RR, norm_RR, compute_morph, oversamp_method, pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)

    compute_morph = {'myMorph', 'u-lbp'} 
    main(multi_mode, 90, 90, do_preprocess, use_weights, maxRR, use_RR, norm_RR, compute_morph, oversamp_method, pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)
     
    use_RR = False
    norm_RR = False
    compute_morph = {'wvlt','HOS', 'u-lbp'} 
    main(multi_mode, 90, 90, do_preprocess, use_weights, maxRR, use_RR, norm_RR, compute_morph, oversamp_method, pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)
            
    compute_morph = {'wvlt','myMorph', 'u-lbp'} 
    main(multi_mode, 90, 90, do_preprocess, use_weights, maxRR, use_RR, norm_RR, compute_morph, oversamp_method, pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)
     
    compute_morph = {'HOS','myMorph', 'u-lbp'} 
    main(multi_mode, 90, 90, do_preprocess, use_weights, maxRR, use_RR, norm_RR, compute_morph, oversamp_method, pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)
     


    # four
    use_RR = True
    norm_RR = True
    compute_morph = {'wvlt', 'HOS', 'u-lbp'} 
    main(multi_mode, 90, 90, do_preprocess, use_weights, maxRR, use_RR, norm_RR, compute_morph, oversamp_method, pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)

    compute_morph = {'wvlt', 'myMorph', 'u-lbp'} 
    main(multi_mode, 90, 90, do_preprocess, use_weights, maxRR, use_RR, norm_RR, compute_morph, oversamp_method, pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)
     

    compute_morph = {'HOS','myMorph', 'u-lbp'} 
    main(multi_mode, 90, 90, do_preprocess, use_weights, maxRR, use_RR, norm_RR, compute_morph, oversamp_method, pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)
     
    use_RR = False
    norm_RR = False
    compute_morph = {'wvlt', 'HOS','myMorph', 'u-lbp'} 
    main(multi_mode, 90, 90, do_preprocess, use_weights, maxRR, use_RR, norm_RR, compute_morph, oversamp_method, pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)
     
    # five
    use_RR = True
    norm_RR = True
    compute_morph = {'wvlt', 'HOS','myMorph', 'u-lbp'} 
    main(multi_mode, 90, 90, do_preprocess, use_weights, maxRR, use_RR, norm_RR, compute_morph, oversamp_method, pca_k, feature_selection, do_cross_val, C_value, gamma_value, reduced_ds, leads_flag)
             
    """

if __name__ == '__main__':
    data_path = '/Users/phild/Data/ecg-classification-mondejar/mitdb'