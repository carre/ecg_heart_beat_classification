#!/usr/bin/env python_code

"""
mit_db.py

Description:
Contains the classes for store the MITBIH database and some utils

VARPA, University of Coruna
Mondejar Guerra, Victor M.
24 Oct 2017
"""
import numpy as np

# Show a 2D plot with the data in beat
def display_signal(beat):
    raise NotImplementedError
    # plt.plot(beat)
    # plt.ylabel('Signal')
    # plt.show()

# Class for RR intervals feature_engineering
class RR_intervals:
    def __init__(self):
        # Instance atributes
        self.pre_r = np.array([])
        self.post_r = np.array([])
        self.local_r = np.array([])
        self.global_r = np.array([])
        
class mit_db:
    def __init__(self):
        # Instance atributes
        self.filename = []
        self.raw_signal = []
        self.beat = np.empty([])  # record, beat, lead
        self.class_id = []
        self.valid_r = []
        self.r_pos = []
        self.orig_r_pos = []
