""" set random seeds for reproducible results"""
import random
random.seed(1)
from numpy.random import seed
seed(1)
from tensorflow.compat.v1 import set_random_seed
set_random_seed(1)

""" import basic libraries """
import os
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

""" import machine learning libraries """
from tensorflow.python.keras.layers.core import Dense, Dropout, Activation
from tensorflow.python.keras.models import Sequential
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from sklearn.preprocessing import StandardScaler
from eli5.permutation_importance import get_score_importances


""" import custom libraries """
from python_code.load_MITBIH import load_mit_db
from python_code.evaluation_AAMI import compute_aami_performance_measures, write_aami_results
from basic_fusion import basic_fusion

"""" this code should be accessed as run_dixon.py --skip_svm_train --skip_svm_fusion_train """


def ann_anwar_tensorflow(data_pth, multi_mode='ovo', do_preprocess=True, oversamp_method='', pca_k=0,
                         feature_selection='', leads_flag=[1, 0], max_rr=True, use_rr=True, norm_rr=True,
                         compute_morph={'HOS', 'wvlt', 'our_morph'}, use_weight_class=True, dropout=0.7,
                         dense_input=256, dense_hidden=32, n_hidden_layers=9, optimizer='adam', epochs=100,
                         batch_size=256, early_stop_patience=30, val_split=0.2, skip_feature_importance=False):

    results_pth = os.path.join(data_pth, 'results')
    perf_measures_path = create_ann_model_name(results_pth + os.sep + 'ann', do_preprocess, max_rr, use_rr,
                                               norm_rr, compute_morph, use_weight_class, feature_selection,
                                               oversamp_method, leads_flag, pca_k, '/')

    # LOAD DATA
    x_train, y_train, x_test, y_test, feature_names = load_features_mondejar(data_pth)

    # get model path
    ann_models_path = os.path.join(data_pth, 'ann_models')
    if not os.path.exists(ann_models_path):
        os.mkdir(ann_models_path)
    ann_models_path = ann_models_path + os.sep + multi_mode + '_rbf'
    ann_models_path = create_ann_model_name(ann_models_path, do_preprocess, max_rr, use_rr, norm_rr, compute_morph,
                                            use_weight_class, feature_selection, oversamp_method, leads_flag, pca_k,
                                            '_')
    ann_models_path = ann_models_path + '.h5'

    # initialize model
    global ann_model
    ann_model = ann_anwar(n_classes=4, n_features=x_train.shape[1],
                          dropout=dropout,
                          dense_input=dense_input,
                          dense_hidden=dense_hidden,
                          n_hidden_layers=n_hidden_layers,
                          optimizer=optimizer)

    # TRAIN MODEL
    callbacks = model_callbacks(ann_models_path, early_stop_patience=early_stop_patience)
    ann_model.fit(x_train, y_train, validation_split=val_split, epochs=epochs, batch_size=batch_size, shuffle=False,
                  callbacks=callbacks, verbose=2)

    # EVALUATE ANN MODEL
    perf_measures = eval_model(ann_model, x_test, y_test, 'ovo_voting_exp', perf_measures_path, ds='test_')
    print('ann model jk-index = {0:.3f}'.format(perf_measures.Ijk))

    # EVALUATE FEATURE IMPORTANCE
    if not skip_feature_importance:
        features_sorted = feature_importance_ann(x_test, y_test, feature_names=feature_names, n_top_features=10)

        keys = features_sorted.keys()
        values = features_sorted.values()
        plt.bar(keys, values)
        plt.ylabel('jk-index contribution')
        plt.savefig(os.path.join(perf_measures_path, 'feature_importance_ann.png'))

    # EVALUATE FUSION MODEL
    norm = False
    features = ['rr', 'wvlt', 'hos', 'our_morph']
    rules = ['sum', 'product', 'majority', 'min', 'max', ]
    for rule in rules:
        basic_fusion(data_pth, features, rule, ann=True, normalize=norm)


def ann_anwar(n_classes, n_features, dense_input, dense_hidden, dropout,n_hidden_layers=9, optimizer='adam',
              loss='categorical_crossentropy'):
    """An artificial neural network architecture based on Anwar et al.
    manuscript: https://doi.org/10.1155/2018/1380348
    """

    model = Sequential()
    model.add(Dense(dense_input, input_shape=(n_features,), activation='relu'))
    for i in range(0, n_hidden_layers):
        model.add(Dense(dense_hidden, activation='relu'))
    model.add(Dropout(dropout))
    model.add(Dense(n_classes))  # sigmoid as output 0 or 1
    model.add(Activation('softmax'))
    model.compile(loss=loss, metrics=['accuracy'], optimizer=optimizer)
    model.summary()
    return model


def create_ann_model_name(model_ann_path, do_preprocess, max_rr, use_rr, norm_rr, compute_morph, use_weight_class,
                          feature_selection, oversamp_method, leads_flag, pca_k, delimiter):
    if leads_flag[0] == 1:
        model_ann_path = model_ann_path + delimiter + 'MLII'

    if leads_flag[1] == 1:
        model_ann_path = model_ann_path + delimiter + 'V1'

    if oversamp_method:
        model_ann_path = model_ann_path + delimiter + oversamp_method

    if feature_selection:
        model_ann_path = model_ann_path + delimiter + feature_selection

    if do_preprocess:
        model_ann_path = model_ann_path + delimiter + 'rm_bsln'

    if max_rr:
        model_ann_path = model_ann_path + delimiter + 'maxRR'

    if use_rr:
        model_ann_path = model_ann_path + delimiter + 'RR'

    if norm_rr:
        model_ann_path = model_ann_path + delimiter + 'norm_RR'

    for descp in sorted(compute_morph):
        if descp:
            model_ann_path = model_ann_path + delimiter + descp

    if use_weight_class:
        model_ann_path = model_ann_path + delimiter + 'weighted'

    if pca_k > 0:
        model_ann_path = model_ann_path + delimiter + 'pca_' + str(pca_k)

    return model_ann_path


def eval_model(model, features, labels_true, voting_strategy, output_path, ds):
    """ Eval the ANN model and export the results

    Notes:
    - The decision function tells us on which side of the hyperplane generated by the classifier we are
      (and how far we are away from it)

    """

    y_pred_prob = model.predict(features)  # for keras model
    labels_predict = np.argmax(y_pred_prob, axis=1)  #

    # decode labels
    labels_true = np.argmax(labels_true, axis=1)

    perf_measures = compute_aami_performance_measures(labels_predict, labels_true)

    # Write results and also predictions on DS2
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    write_aami_results(perf_measures, output_path + '/' + ds + '_score_Ijk_' +
                       str(format(perf_measures.Ijk, '.2f')) + '_' + voting_strategy + '.txt')

    # # Array to .csv
    np.savetxt(output_path + '/' + ds + 'y_pred_prob.csv', y_pred_prob)
    np.savetxt(output_path + '/' + ds + 'predict_' + voting_strategy + '.csv', labels_predict.astype(int), '%.0f')

    print("Results written to " + output_path + '/' + ds)

    return perf_measures


def model_callbacks(model_file, early_stop_monitor='val_accuracy', early_stop_patience=20,
                    model_checkpoint_monitor='val_loss'):

    callbacks = []

    if 'loss' in early_stop_monitor:
        mode = 'min'
    else:
        mode = 'max'

    callbacks.append(EarlyStopping(monitor=early_stop_monitor, mode=mode, patience=early_stop_patience, verbose=1))

    if 'loss' in model_checkpoint_monitor:
        mode = 'min'
    else:
        mode = 'max'

    callbacks.append(ModelCheckpoint(model_file, monitor=model_checkpoint_monitor, mode=mode, save_best_only=True,
                                     verbose=1))

    return callbacks


def load_features_mondejar(data_pth, max_rr=True, use_rr=True, norm_rr=True, do_preprocess=True, reduced_DS=False,
                           compute_morph={'HOS', 'wvlt', 'our_morph'}, win_l=90, win_r=90, leads_flag=[1, 0],
                           balance_classes=False):
    """ loads full feature set for best model (RR, HOS, wvlt, out_morph) based on Mondejar et al. 2019 """

    # load train data
    [x_train, y_train, _, feature_names] = load_mit_db('DS1', win_l, win_r, do_preprocess, max_rr, use_rr, norm_rr,
                                                       compute_morph, data_pth, reduced_DS, leads_flag)

    # Load test data
    [x_test, y_test, _, _] = load_mit_db('DS2', win_l, win_r, do_preprocess, max_rr, use_rr, norm_rr, compute_morph,
                                         data_pth, reduced_DS, leads_flag)

    # scale features
    scaler = StandardScaler()
    scaler.fit(x_train)
    x_train = scaler.transform(x_train)
    x_test = scaler.transform(x_test)

    if balance_classes:
        col_names = ['feature_' + str(i) for i in range(0, x_train.shape[1])]
        col_names.append('label')
        df = pd.DataFrame(data=np.hstack((x_train, np.expand_dims(y_train, axis=1))), columns=col_names)

        count_class_0, count_class_1, count_class_2, count_class_3 = df.label.value_counts()
        df_class_0 = df[df['label'] == 0]
        df_class_1 = df[df['label'] == 1]
        df_class_2 = df[df['label'] == 2]
        df_class_3 = df[df['label'] == 3]

        df_class_0_under = df_class_0.sample(count_class_1, random_state=1)
        df_under = pd.concat([df_class_0_under, df_class_1, df_class_2, df_class_3], axis=0)
        print('Random under-sampling:')
        print(df_under.label.value_counts())
        x_train = df_under[col_names[0:-1]].values
        y_train = df_under[col_names[-1]].values

    # prepare labels for keras
    y_train = to_categorical(y_train, 4)
    y_test = to_categorical(y_test, 4)

    return x_train, y_train, x_test, y_test, feature_names


def feature_importance_ann(features, labels, feature_names=None, verbose=True, n_top_features=5):
    """ calculate feature importance for a black box model
    https://towardsdatascience.com/how-to-find-feature-importances-for-blackbox-models-c418b694659d
    https://github.com/cerlymarco/MEDIUM_NoteBook/blob/master/NeuralNet_FeatureImportance/NeuralNet_FeatImportance.ipynb

    """

    score_fn = jk_index_score

    if np.ndim(labels) > 1:
        labels = np.argmax(labels, axis=1)

    base_score, score_decreases = get_score_importances(score_fn, np.array(features), labels)
    feature_importances = np.mean(score_decreases, axis=0)

    feature_importance_dict = {}
    for i, feature_name in enumerate(feature_names):
        feature_importance_dict[feature_name] = feature_importances[i]

    features_sorted = dict(sorted(feature_importance_dict.items(), key=lambda x: x[1], reverse=True)[:n_top_features])

    if verbose:
        print('\nTop {} features:'.format(n_top_features))
        for key, value in features_sorted.items():
            print('{0} with value {1:.3f}'.format(key, value))

    return features_sorted


def jk_index_score(x, y):
    y_pred = ann_model.predict(x)
    if np.ndim(y_pred) > 1:
        y_pred = np.argmax(y_pred, axis=1)
    perf_measures = compute_aami_performance_measures(y_pred, y)
    ijk = perf_measures.Ijk
    if np.isnan(ijk):
        ijk = 0
    return ijk


if __name__ == '__main__':
    DATA_PTH = '/Users/phild/Data/ecg-classification-mondejar/mitdb/'
    ann_anwar_tensorflow(DATA_PTH)


